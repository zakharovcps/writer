const { connect, StringCodec, Empty, headers } = require("nats");
const fs = require('fs').promises;
const path = require('path');

const http = require('http');
const WebSocketServer = require('websocket').server;

// create an encoder
//const sc = StringCodec();

const serverInfo = { server: "localhost", port: 4222, name: 'writer' }; //, pingInterval: 10000, maxPingOut: 2, },
const transport = ['nats', 'websocket'][0];

const log = function () {
    const args = [new Date().toJSON().split('T')[1].split('.')[0]]; // только время
    for (let i = 0; i < arguments.length; i++) {
        args.push(arguments[i]);
    }
    console.log.apply(console, args);
};

// websocket
if (transport == 'websocket') {
    (async () => {
        const server = http.createServer(function(request, response) {
            console.log('Received request for ' + request.url);
            response.writeHead(404);
            response.end();
        });
        server.listen(8080, function() {
            console.log('Server is listening on port 8080');
        });

        const wsServer = new WebSocketServer({
            httpServer: server,
            // You should not use autoAcceptConnections for production
            // applications, as it defeats all standard cross-origin protection
            // facilities built into the protocol and the browser.  You should
            // *always* verify the connection's origin and decide whether or not
            // to accept it.
            autoAcceptConnections: false
        });

        wsServer.on('request', function(request) {
            const connection = request.accept('echo-protocol', request.origin);

            let fh;
            let fileName, fileType, filePath;

            log('Connection accepted.');
            connection.on('message', function(message) {
                // принимаем запрос от клиента
                let action;
                let buf;
                let offset;
                if (message.type === 'utf8') {
                    // если текст, значит json с командой
                    try {
                        let rec = JSON.parse(message.utf8Data);
                        ({ action, fileName, fileType } = rec);
                    } catch {
                        action = undefined;
                    }
                    //log('Received Message: ' + message.utf8Data);
                    //connection.sendUTF(message.utf8Data);
                }
                else if (message.type === 'binary' && message.binaryData?.length >= 4) {
                    // если бинарный, значит данные файла
                    action = 'write';
                    offset = message.binaryData.readInt32LE(0);
                    buf = message.binaryData.slice(4);
                    //log('Received Binary Message of ' + message.binaryData.length + ' bytes');
                    //connection.sendBytes(message.binaryData);
                }

                // переход в асинк
                (async () => {
                    switch (action) {
                        case 'open':
                            filePath = path.join('./files', fileName);
                            log(`S: открываем на запись "${fileName}", тип: ${fileType}`);
                            fh = await fs.open(filePath, 'w+');
                            connection.sendUTF('ok');
                            break;
                        case 'write':
                            log(`S: получили блок с позиции ${offset}, байт: ${buf.length}`);
                            // задержка обработки
                            await new Promise(resolve => setTimeout(resolve, 1000));
                            // запись в участок файла
                            await fh.write(buf, 0, buf.length, offset);
                            connection.sendUTF('ok');
                            break;
                        case 'close':
                            log(`S: закрываем и завершаем`);
                            fh = await fh.close(); // undefined
                            connection.sendUTF('ok');
                            break;
                        default:
                            log('Получено неподдерживаемое сообщение');
                            connection.sendUTF('Неподдерживаемое сообщение');
                    }
                })()
                .then(() => {})
                .catch(err => {
                    log(err);
                });
            });
            connection.on('close', function(reasonCode, description) {
                log('Peer ' + connection.remoteAddress + ' disconnected.');
                log(`Код завершения: ${reasonCode}, причина: ${description}`);
            });
        });
    })().then(async () => {
        
    }).catch(async err => {
        log(err);
    });
} else if (transport == 'nats') {
    // nats
    (async () => {
        // подключаемся к nats
        const nc = await connect(serverInfo);

        // слушатель (писатель)
        const sub = nc.subscribe("writer.*");
        (async (sub) => {
            log(`S: начинаем слушать ${sub.getSubject()}`);
            let response;
            let fh;
            let pos;
            let fileName, filePath;
            let isSubscribed = true;
            // ожидаем и обрабатываем входящие сообщения
            while(isSubscribed) {
                for await (const msg of sub) {
                    response = '';
                    // определяем действие
                    try {
                        switch (msg.subject.split('.')[1]) {
                            case 'open':
                                fileName = msg.headers?.get('Name');
                                filePath = path.join('./files', fileName);
                                log(`S: открываем на запись "${fileName}", тип: ${msg.headers?.get('Content-Type')}`);
                                fh = await fs.open(filePath, 'w+');
                                break;
                            case 'write':
                                pos = msg.headers?.get('Offset');
                                log(`S: получили блок с позиции ${pos}, байт: ${msg.data?.length}`);
                                // задержка обработки
                                await new Promise(resolve => setTimeout(resolve, 1000));
                                // запись в участок файла
                                await fh.write(msg.data, 0, msg.data.length, pos);
                                break;
                            case 'close':
                                log(`S: закрываем и завершаем`);
                                fh = await fh.close(); // undefined
                                isSubscribed = false;
                                await sub.unsubscribe(); // отписка
                                //await nc.drain(); // закрытие подключения
                                break;
                        }
                    } catch (e) {
                        response = e.message;
                    }
                    // обратное письмо
                    if (msg.respond(Buffer.from(response))) {
                        log(`S: ответили #${sub.getProcessed()}`);
                    } else {
                        log(`S: НЕ ответили #${sub.getProcessed()} - нет ответного контекста`);
                    }
                }
                // пауза повторного ожидания
                await new Promise(resolve => setTimeout(resolve, 250));
            }

            await nc.drain();

            log(`S: отключение (drain) ${sub.getSubject()}, писатель принял файл`);
        })(sub);

        return nc;
    })().then(async nc => {
        //let err = await nc.drain();
        //log(err || 'Писатель принял файл');
    }).catch(async err => {
        log(err);
        try {
            await nc.close();
        } catch (err) { }
    });
}
